var geocoder;
var marker;
var image = '../images/pin.png';
var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.

  function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 6
  });
  var infoWindow = new google.maps.InfoWindow({map: map});

  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
      infoWindow.setContent('Location found.');
      map.setCenter(pos);
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }
  }

  function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
  }
}

        marker = new google.maps.Marker({
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
          position: {lat: 49.7167, lng: 19.3167},
          icon: {
              url: "images/pin.png"
            }
        });
        marker.addListener('click', toggleBounce);
      }


      // function toggleBounce() {
      //   if (marker.getAnimation() !== null) {
      //     marker.setAnimation(null);
      //   } else {
      //     marker.setAnimation(google.maps.Animation.BOUNCE);
      //   }
      // }


$("#map").gmap3({
  marker:{
    latLng: [46.578498,2.457275],
    options:{
      draggable:true
    },
    events:{
      dragend: function(marker){
        $(this).gmap3({
          getaddress:{
            latLng:marker.getPosition(),
            callback:function(results){
              var map = $(this).gmap3("get"),
                infowindow = $(this).gmap3({get:"infowindow"}),
                content = results && results[1] ? results && results[1].formatted_address : "no address";
              if (infowindow){
                infowindow.open(map, marker);
                infowindow.setContent(content);
              } else {
                $(this).gmap3({
                  infowindow:{
                    anchor:marker,
                    options:{content: content}
                  }
                });
              }
            }
          }
        });
      }
    }
  },
  map:{
    options:{
      zoom: 5
    }
  }
});
