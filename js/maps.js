

      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.

function initMap() {
var geocoder;
var person;
var image = '../images/pin.png';
var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 15,
    scrollwheel: false,
    draggable:true,
    mapTypeControl: true,
      mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_LEFT
  },
  zoomControl: true,
  zoomControlOptions: {
      position: google.maps.ControlPosition.LEFT_CENTER
  },
  scaleControl: true,
  fullscreenControl: true
  });

function setPersonMarkers(map) {
var marker = new google.maps.Marker({
  map: map,
  draggable: true,
  animation: google.maps.Animation.DROP,
  icon: {
      url: "images/pin.png"
    }
});

var input = document.getElementById('pac-input');
var searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);


// Bias the SearchBox results towards current map's viewport.
map.addListener('bounds_changed', function() {
  searchBox.setBounds(map.getBounds());
});

// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener('places_changed', function() {
  var places = searchBox.getPlaces();
  if (places.length == 0) {
    return;
  }

  // For each place, get the icon, name and location.
  var bounds = new google.maps.LatLngBounds();
  places.forEach(function(place) {
    if (!place.geometry) {
      console.log("Returned place contains no geometry");
      return;
    }
    if (place.geometry.viewport) {
      // Only geocodes have viewport.
      bounds.union(place.geometry.viewport);
      marker.setPosition(place.location)
    } else {
      bounds.extend(place.geometry.location);
      marker.setPosition(place.location)
    }
  });
  map.fitBounds(bounds);
});

var infowindow = new google.maps.InfoWindow;
google.maps.event.addListener(marker, 'dragend', function () {
  map.setCenter(this.getPosition());
  geocodePosition(marker.getPosition(),marker,infowindow);

});


google.maps.event.addListener(map, 'dragend', function () {
  marker.setPosition(this.getCenter());  // set marker position to map center
});

google.maps.event.addListener(map, 'click', function () {
  marker.setPosition(this.getCenter()); // set marker position to map center
});

google.maps.event.addListener(infowindow, 'domready', function(){
         $('.modal-trigger').leanModal();
     });

google.maps.event.addListener(marker, 'click', function () {
  geocodePosition(marker.getPosition(),marker,infowindow);

});
  // Try HTML5 geolocation.
if (navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(function(position) {
    var pos = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    };
    map.setCenter(pos);
    marker.setPosition(pos);

  }, function() {
    handleLocationError(true, map.getCenter());
  });
} else {
  // Browser doesn't support Geolocation
  handleLocationError(false, map.getCenter());
}
  }

function geocodePosition(pos,marker,infowindow) {
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      marker.formatted_address = responses[0].formatted_address;
    } else {
      marker.formatted_address = 'Cannot determine address at this location.';
    }
    infowindow.setContent('<div class="card blue-grey darken-1">' +
                            '<div class="card-content white-text">' +
                              '<span class="card-title">' + marker.formatted_address + '</span>' +
                              '<p>If you want to move from this address, look for the driver and in the way</p>' +
                            '</div>' +
                            '<div class="card-action">' +
                              '<a href="#modal1"  class="show_hide waves-effect waves-light btn modal-trigger">Search Driver </a>' +
                            '</div>'+
                          '</div>');
    infowindow.open(map, marker);
  });

  function isInRange(position) {
    return google.maps.geometry.spherical.computeDistanceBetween (position, marker.position) < 5000;
  }

function filterByDistance(driver) {
  if (isInRange(driver.position)) {
    return true;
  }
  return false;
}
function timeToYou(marker,driver){
var directionsService = new google.maps.DirectionsService();
    var request = {
        origin: marker.position,
        destination: driver.position,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            var route = response.routes[0];
            // duration plus 15 minutes
            duration = response.routes[0].legs[0].duration.value + 15*60;
            document.getElementById("duration").innerHTML += '<p>' + response.routes[0].legs[0].duration.text + '</p><br />';
        } else {
            window.alert('Please enter a starting location');
        }
    });
}
  var avalibleDrivers = drivers.filter(filterByDistance);
  document.getElementById("duration").innerHTML = "";
  document.getElementById('drivers').innerHTML = "";
  document.getElementById("Drive").innerHTML = "";
    for(var i = 0; i < avalibleDrivers.length; i++) {
      document.getElementById("Drive").innerHTML += '<span><a href= "#!"  class="drive btn-large waves-effect waves-light blue-grey darken-3 "><strong>Take Me</strong></a></span><br />';
      timeToYou(marker,avalibleDrivers[i])
      if(typeof(avalibleDrivers[i]) != 'undefined')
          document.getElementById('drivers').innerHTML += '<h5>' + avalibleDrivers[i].title + '</h5><br />';
    }
   }

  var driversPositions = {
   'John':  [ 50.470887, 19.072684],
    'Simon': [ 50.27233638, 19.04643059],
    'Smis': [50.26663084, 18.99098396],
    'Gal':  [ 50.25071752, 18.99630547],
    'Man': [ 50.25861997, 19.03969288],
    'man': [ 50.2458876, 19.02784824],
 };

 var drivers = [];
 function setDriver(map, locations) {
 for (var key in driversPositions) {
    var data = driversPositions[key];
    var driver = new google.maps.Marker({
        position: new google.maps.LatLng (data[0], data[1]),
        map: map,
        title: key
    });
     drivers.push(driver)
  }
}



setDriver(map,driversPositions);
setPersonMarkers(map);


}


